class TypeLab {
  constructor(element, typeDelay = 200) {
    if (!element) throw new ReferrenceError('TypeLab element must be defined');
    this.element = element;
    this.typeDelay = typeDelay;
  }

  update(newValue = '') {
    let deletionCommands, additionCommands;
    let oldValue = this.element.textContent.trim() || '';
    newValue = newValue.trim();

    [deletionCommands, oldValue] = this.getDeletions(newValue, oldValue);
    [additionCommands] = this.getAdditions(newValue, oldValue);

    return new Promise((resolve, reject) => {
      this.animate(deletionCommands.concat(additionCommands), resolve);
    });
  }

  getDeletions(newValue = '', oldValue = '') {
    if (newValue.startsWith(oldValue)) return [[], oldValue];
    const diffLength = TypeLab.calculateDiffLength(newValue, oldValue);
    return [new Array(diffLength).fill(-1), oldValue.slice(0, -diffLength)];
  }

  getAdditions(newValue = '', oldValue = '') {
    return [newValue.replace(new RegExp(`^${oldValue}`), '').split(''), newValue];
  }

  animate(commandArray = [], resolve, index = 0) {
    const command = commandArray[index];
    if (!command) return resolve();

    const oldTextContent = this.element.textContent.trim();
    const newTextContent = command === -1 ? oldTextContent.slice(0, -1) : oldTextContent + command;

    this.element.textContent = newTextContent;
    return setTimeout(this.animate.bind(this, commandArray, resolve, index + 1), this.typeDelay);
  }

  static calculateDiffLength(newValue = '', oldValue = '') {
    let diffLength = oldValue.split('').findIndex((character, index, characterArray) => {
      if (index === 0) return false;
      const testValue = characterArray.slice(0, -Math.abs(index)).join('');
      return newValue.startsWith(testValue);
    });

    if (diffLength === -1) return oldValue.length;
    return diffLength;
  }
}